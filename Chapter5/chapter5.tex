\chapter{Evaluation}

\epigraph{``Why Evaluate Software Quality?\\
1.~The software product may be hard to understand and difficult to modify.\\
2.~The software product may be difficult to use, or easy to misuse.\\
3.~The software product may be unnecessarily machine-dependent, or hard to
integrate with other programs.''}
{Barry W. Boehm~\cite{boehm1976quantitative}}

\noindent
\aflak{} was evaluated on a middle-end light laptop running Debian~9 with an
Intel\textregistered{} Core\texttrademark{} i7-7560U CPU @ 2.40GHz processor
and an integrated GPU chipset. \aflak{} is supported on all mainstream
operating systems (Linux, macOS and Windows~10).

\section{Checking compliance to standards}

\label{sec:compliance}

\aflak{} connected to the GAVO SIAP 2.0 repository
and issued a query to retrieve a dataset at sky coordinates $(161.0, -59.7)$\footnote{First
component is right-ascension, second component is declination.}.
For the same query, the same result as with PyVO is gotten.
A one-gigabyte dataset from another SIAP 1.0 repository at GAVO was as well queried,
and apart from the initial download duration the first time \aflak{} attempts
to download the dataset, all individual transforms run in less than a second.
With the hardware stated above, all animations run perfectly smoothly
(see Figure~\ref{fig:gb-image}).

\begin{figure}
    \makebox[\textwidth][c]{
        \includegraphics[width=1.1\textwidth]{Chapter5/figures/gb-image.png}
    }
    \centering
    \caption{
      \label{fig:gb-image}
      Image with a size in the gigabyte range queried from GAVO SIAP 1.0 repository,
      as shown within \aflak{}. SIAP 1.0 dates from around 2002 and does
      not support any sort of ``ID'' for datasets.
      GAVO SIAP 1.0: \url{http://dc.zah.uni-heidelberg.de/hppunion/q/im/siap.xml}
    }
\end{figure}

FITS files exported with \aflak{} were tested with FITS viewers such as SAOImage~DS9 and
checked for strict compliance with the FITS standard using the Astropy checking tools.
The exported FITS files can of course be re-loaded back into \aflak{}.

\aflak{}'s code is free and available on the following link: \mbox{\url{https://github.com/aflak-vis/aflak}}. \\
The implementation of the VO standards used
by \aflak{} can be found hereafter: \\ \mbox{\url{https://github.com/aflak-vis/vo}}


\section{First use case: Equivalent width}

\subsection{Introduction to \emph{human-in-the-loop} concept}

Figure~\ref{fig:human-in-the-loop} shows the visualization discovery process
that \aflak{} strives to achieve.
\aflak{} is designed ``not to replace the human but to keep the human in the
loop by extending human capabilities.'' By allowing fast iteration (and
prototyping) with varying input datasets and algorithms, \aflak{} makes
\emph{astronomer-in-the-loop} a reality. The arrows in
Figure~\ref{fig:teaser}, \aflak{}'s teaser figure, explicitly shows this
feedback loop in that the astronomer can
at any time, by the sight of a visualized output, either go back and choose
to re-query another input datasets, update the analytics pipeline in
the visual program, or simply fine-tune parameters in the same visual
program.
While any of the above-mentioned action is done, the output values of the
current visual program and thus the content of the output window is updated
in real time.
The next sections will give a specific example, that of the computing of
equivalent widths, to prove the effectiveness of the feedback loop provided
to the astronomer by \aflak{}.
Equivalent widths are an interesting subject in that they include several
parameters that require manual and gradual tuning before a satisfactory
result can be obtained.

\begin{figure}
 \centering
 \includegraphics[width=\textwidth]{Chapter5/figures/human-in-the-loop.png}
 \caption{
     The visualization discovery process as presented by Johnson \emph{et
     al.}~\citep{johnson2005nih}.
     This figure represents the core visualization concept of
     \emph{human-in-the-loop}.
 }
 \label{fig:human-in-the-loop}
\end{figure}

\subsection{Use case}

\label{sec:equivalent_width}

Figure~\ref{fig:equivalent} shows an example of workflow extracting the
equivalent width using the same computational method as Matsubayashi et
al.~\cite{matsubayashi2011spatially}.
Equivalent width can be defined as follows:
``The equivalent width of a spectral line is a measure of the area of the line
on a plot of intensity versus wavelength. It is found by forming a rectangle
with a height equal to that of continuum emission, and finding the width such
that the area of the rectangle is equal to the area in the spectral line. It
is a measure of the strength of spectral features that is primarily used in
astronomy''~\cite{carroll2007introduction}.
In Figure~\ref{fig:equivalent}, the dataset is loaded with the ``open\_fits''
node~\#2 and the ``fits\_to\_image'' node~\#3. The continuum emission is
computed on both sides of the emission line by the ``average'' nodes~\#5 (left side)
and~\#6 (right side). Then the average value of the spectral line is computed
by the ``average'' node~\#4. From then, equivalent width is computed by
node~\#8. The computed equivalent width is shown in output~\#3.

Computing the equivalent width requires a lot of iterations to choose the
``just right'' threshold wavelengths on computing continuum emission and the
area of the spectral line. One interviewed astronomer reported that \aflak{}
allows to quickly find the appropriate thresholds thank to the quick feedback
loop. If we exclude negligible floating-point rounding errors, results
obtained with PyRAF and \aflak{} are perfectly consistent.

\section{Second use case: Velocity field map}

\label{sec:field_map}

\begin{sidewaysfigure}
    \makebox[\textwidth][c]{
        \includegraphics[width=\textwidth]{Chapter5/figures/velocity-field-map.eps}
    }
    \centering
    \caption{
      \label{fig:velocity}
      A node graph for computing the velocity field map using the effect of
      Doppler shift on an emission line. It generates two field maps (one in
      output~\#1 and one in output~\#2) with two different computation methods.
      Average value of the image around the emission line in shown in output~\#3.
      We see that there is a fast moving object on the lower right-hand side.
    }
\end{sidewaysfigure}

Figure~\ref{fig:velocity} shows a very simple program demonstrating how an
astronomer can select a band on which there is an emission line (here
from frame index~3135 to~3155).
Similar to the first use case, the astronomer can select the appropriate
thresholds for the band on which a velocity field map will be created and
quickly try different computation methods.

\section{Comparison with current tools}

%Do the same by changing a FITS file/ changing a parameter.
Sections~\ref{sec:equivalent_width} and~\ref{sec:field_map} reviewed two analytic use cases for using \aflak{}.
But such analytic features would be irrelevant without the ingrained support
for reproducibility, implemented by embedding provenance data in exported FITS files
whose compliance is checked in section~\ref{sec:compliance}.
Bringing about \emph{complete} reproducibility of \emph{non-linear} analysis from a standard-compliant FITS
file while maintaining interoperability with existing software is a first.
This method has the advantage of simplicity in that it relies on widely supported
standards and does not multiply the number of files a researcher needs to handle.

For example, we can compare with PyRAF~\cite{de2001pyraf}, which is a tool that provides a
shell-like command prompt to analyze FITS datasets. Roughly, for each
\aflak{} node an astronomer would need to manually run a PyRAF command. A PyRAF
command takes at least two arguments: an input FITS file and an output FITS
file. All the working files are saved and read on disk. As you may guess, the
more PyRAF commands are run, the more FITS files appear in your working
directory, making it very confusing to maintain the provenance of each
file. Assuming that a diligent astronomer kept the history of the PyRAF
commands somewhere in a text file, each command would take a few seconds to
run, thus the whole process of going through a graph like that in
Figure~\ref{fig:equivalent} would take about a minute. What's more, the
astronomer would need to restart the process from scratch as soon as they
need to change a parameter. Thanks to \aflak{}'s interactive interface, as soon
as the node graph is built once, a user can seamlessly
compare two different results by slightly modifying a single parameter (in a
transformation, a query or input file). This can lead to new insights and
discoveries.

But that is not all, thanks to the ability to download datasets from data repositories,
there is no need to always keep a local copy of the data an astronomer wants to analyze (and remember where it is located).
Each piece of data is uniquely identified by its \texttt{obs\_publisher\_did} and can be re-downloaded on demand.
Query parameters can be changed with a few mouse movements and the result of the
same processing on another input can be visualized after the new dataset is
downloaded, or immediately if the queried dataset is cached on the disk.
One interviewed domain expert reported that this is a non-negligible leap
forward in terms of workflow management.

\subsection{In-depth comparison}

Listing~\ref{lst:bashiraf} shows a commented Bash/IRAF script that
computes the equivalent width of a datacube, using the same method as the
node editor outlined in Figure~\ref{fig:equivalent}.

\begin{minipage}{\linewidth}

\begin{lstlisting}[caption={Extracting equivalent width with Bash/IRAF}, label={lst:bashiraf}]
\end{lstlisting}

\noindent { \footnotesize \texttt{ \# Sub-datacube for each band (in Figure~\ref{fig:equivalent}: nodes \#3, \#4 and \#5)} }

\begin{lstlisting}[language=bash]
awk 'BEGIN{for (i = 3099; i < 3119; i++) {
    printf ("manga-8454-12703-LINCUBE.fits[1][,,%d]\n", i)}
  }' > file-off1.list
awk 'BEGIN{for (i = 3134; i < 3154; i++) {
    printf ("manga-8454-12703-LINCUBE.fits[1][,,%d]\n", i)}
  }' > file-on.list
awk 'BEGIN{for (i = 3174; i < 3194; i++) {
    printf ("manga-8454-12703-LINCUBE.fits[1][,,%d]\n", i)}
  }' > file-off2.list
\end{lstlisting}

\noindent \texttt{ \# Then compute average on each band}

\begin{lstlisting}[language=bash]
imcomb @file-off1.list off1-average.fits combine=average
imcomb @file-on.list on-average.fits combine=average
imcomb @file-off2.list off2-average.fits combine=average
\end{lstlisting}

\noindent { \small \texttt{ \# Combine off-band images (in Figure~\ref{fig:equivalent}: nodes \#7 and \#8)} }

\begin{lstlisting}[language=bash]
echo "0.533333" > scale-off.dat
echo "0.466667" >> scale-off.dat
imcomb off1-average.fits,off2-average.fits off-average.fits \
  combine=average weight=@scale-off.dat
\end{lstlisting}

\noindent \texttt{ \# Make equivalent-width map (in Figure~\ref{fig:equivalent}: node \#9)}

\begin{lstlisting}[language=bash]
imarith off-average.fits - on-average.fits off-on-average.fits
imarith off-on-average.fits * 20 flux.fits
imarith flux.fits / off-average.fits equivalent-width.fits
\end{lstlisting}

\end{minipage}

\vspace{2em}

As the reader may infer from the above code, each operation is creating files on
the file system, while subsequent operations are using the created files.
The content of the intermediary files can only be seen using dedicated viewers
such as DS9. With the example of equivalent widths, many constants must be
precisely adjusted. Every time a constant is changed, all the next cascading
operations must be re-run. Not only this process is time-consuming and
error-prone---enlarging an already too long feedback loop---, but provenance
is as well far from being managed. Out of the many files generated in the
file system, how does the user remember how each individual file was generated?
By comparison, \aflak{} here shines by its fast feedback loop (less than a
second is needed to refresh the visualizations in an output window after a
change in the node editor) and automatic management of provenance.
Importantly, if we exclude negligible floating-point rounding errors, results
obtained with PyRAF and \aflak{} are perfectly consistent.

\subsection{Equivalent width with a macro}

\begin{sidewaysfigure}
  \includegraphics[width=\textwidth]{Chapter5/figures/macro_equivalent_width.eps}
  \caption{
      Example of using an \aflak{} macro to compute equivalent width. The
      macro encapsulates all the logic implemented as shown in the node editor
      in Figure~\ref{fig:equivalent}, only exposing the relevant constants that
      the astronomers are expected to gradually adjust until they get a
      satisfactory outcome.
  }
  \label{fig:macro_equivalent_width}
\end{sidewaysfigure}

The previous section compares existing tools with \aflak{}. This section
gives a concrete example of macro usage for equivalent widths.
Figure~\ref{fig:macro_equivalent_width} shows an example of a macro
implementing the computation of equivalent widths, the same processing as the
one done in Figure~\ref{fig:equivalent}.
The advantage of using macros are clear: visual clutter on the original node
interface is widely reduced, while the computing speed is not impacted for a
423~MB input dataset. Only the input datacube and the constant parameters
that are relevant in computing the equivalent width are exposed by the macro.
Through the sharing and the export of macros such as this one given as
example, common parameterized analytics possibilities are only a few clicks away.

\section{Advantages of provenance management in a visual context
not limited to astronomy}

Some astronomers highlight the importance of provenance management. A
provenance data model standard is currently in development by the
\emph{International Virtual Observatory Alliance}, whose name is
\emph{ProvenanceDM}~\citep{servillat2019provenance}. A representation of the
data recorded through provenance is shown in Figure~\ref{fig:provenancedm}.
\aflak{} can be used to reproduce an analysis pipeline, from the data source
to the final output, following a model very similar to that of
\emph{ProvenanceDM}.
As we consider the lists of use cases that \emph{ProvenanceDM} must fulfill, we can see that
\aflak{} successfully addresses each of them as well, so we can conclude that
\aflak{} provides a \emph{reference implementation} of the \emph{ProvenanceDM}
data model, as defined by the \emph{International Virtual Observatory Alliance}.

\begin{description}
  \item[Traceability of products:] ``Track the lineage of a product back to
    the raw material (backwards search), show the workflow or the dataflow
    that led to a product.'' \aflak{}'s visual approach of representing data
    flow with a visualized directed acyclic graph meets this use case.
  \item[Acknowledgment and contact information:] ``Find the people involved
    in the production of a dataset, the people/organizations/institutes that
    one may want to acknowledge or can be asked for more information.''
    \aflak{}'s nodes contain information about their author, thus \aflak{}
    meets this use case.
  \item[Quality and reliability assessment:] ``Assess the quality and
    reliability of an observation, production step or dataset.'' The version
    and the unique UUIDs of the nodes used in \aflak{} to generate any
    dataset are logged, which allows to check quality and reliability at any
    time in the future.
  \item[Identification of error location:] ``Find the location of possible
    error sources in the generation of a product.'' All errors that occur
    during processing on \aflak{} are logged and a detailed stack trace is
    shown to thus user, thus fulfilling this requirement.
  \item[Search in structured provenance metadata:] ``Use provenance criteria
    to locate datasets (forward search), e.g. finding all images produced by
    a certain processing step or derived from data which were taken by a
    given facility.'' It is theoretically possible to classify FITS files that
    were exported by \aflak{} grouped by the exact node editor that generated
    them, as a serialized node editor is included in all generated FITS
    files. Thus this requirement is met.
\end{description}

Besides, Wilkinson \emph{et al.} define the FAIR principles for data sharing for
\emph{all} fields of science as ``Findable, Accessible, Interoperable,
Re-usable''~\citep{wilkinson2016fair}, which is used as a design principle in
the development of \emph{ProvenanceDM}. \aflak{}'s macro fulfills the need
for re-usability. Moreover, findability, accessiblity and interoperability of
datasets and methods are fulfilled by the implementation of Virtual
Observatory standards for data retrieving and exchange (SIA, FITS format,
etc.). Section~\ref{sec:more-improvements} hints at the planned feature of
implementing a sharing platform and repository for community-contributed
nodes, which can then seamlessly be downloaded and extend \aflak{} with new
analytical features. Such open repository would fulfill even more the accessiblity requirements of the FAIR principles.

\begin{figure}
  \includegraphics[width=\textwidth]{Chapter5/figures/provenancedm.png}
  \caption{
      An example graph of provenance discovery. Starting with a released
      dataset (left), the involved activities (blue boxes), progenitor
      entities (yellow rounded boxes) and responsible agents (orange
      pentagons) are discovered.~\citep{servillat2019provenance}
  }
  \label{fig:provenancedm}
\end{figure}

In any case, provenance management allows to keep track of whatever was done
to come to a conclusion. All the trial and error of the researches that led
them to the answer would be recorded as well. Thanks to this, we make
possible the end-to-end reproducibility of any investigation. If an
investigation were to be re-opened, it is not infrequent that a new
investigation must be conducted. We could thus check exactly how the past
investigation was done and improve the current search. The exact same needs
can be found in forensic science.
A framework dealing with forensic data thus has everything to gain from
implementing end-to-end data provenance management, and \aflak{}'s
methodology to managing astronomical data provenance could as well be applied
to forensic data provenance.
Figure~\ref{fig:lmml-provenance} represents how a graph-like provenance data
model can be applied to a forensic case.

\begin{figure}
    \centering
    \includegraphics[width=0.5\linewidth]{Chapter5/figures/provenance.png}
    \caption{
        Provenance in forensics: Reconstitution of crime according to different
        \mbox{hypotheses.}
    }
    \label{fig:lmml-provenance}
\end{figure}


\section{Distribution and recognition}

A piece of software without users and a community is meaningless. While
developing \aflak{}, the author attempted to build a community around it. And even
when this Ph.\,D.\ is finished, the desire to continue \aflak{}'s development will still be there. One
could say that this Ph\,D.\ was only an alibi to start this project.

In any case, this is not much, but \aflak{}'s GitHub page has accumulated 12 stars on GitHub at
the time of writing (see Figure~\ref{fig:stargazers} to see \aflak{}'s
stargazers\footnote{Screenshot taken at
\url{https://github.com/aflak-vis/aflak/stargazers} on July
7\textsuperscript{th}, 2019}.), along with 15 followers to \aflak{}'s newsletter,
run by this thesis's author with Mailchimp\footnote{A marketing automation
platform and an email marketing service at \url{https://mailchimp.com/}.}.

\begin{figure}
  \makebox[\textwidth][c]{
    \includegraphics[width=1.2\textwidth]{Chapter5/aflak-stargazers.png}
  }
  \caption{
      \aflak{}'s repository has 12 stargazers. We never attempted to do
      any kind of pro-active campaign to earn stars.
  }
  \label{fig:stargazers}
\end{figure}

We as well went to many astronomical events to give oral presentation and
talks. We received interested feedback from many people and even had the
chance to be invited in the 2019 Integral Field Spectroscopy Study
Group\footnote{``Integral Field Spectroscopy Study Group'' is the author's
rough English translation of this conference's original Japanese name, which
is 面分光研究会２０１９ ---新面分光装置で花開く新しいサイエンス---.}, that will be held in November 2019.

We attempt to reduce the hurdle as low as possible for people starting to use
\aflak{}. Some astronomical software may be very cumbersome to install. As a
matter of fact, the author was never able to get an IRAF installation running
on his own machine. TOPCAT caused a few headaches and fiddling with the
author's Java environment before they could get it up and running.
\aflak{} boasts to be downloadable and installable from source with a single
command---a one-liner---, as demonstrated in Listing~\ref{lst:install-aflak}.

\newpage

\begin{lstlisting}[
    language=bash,
    caption={Installing \aflak{}: A one-liner.},
    label={lst:install-aflak}
]
# Install Rust (if you already have rust, skip this step)
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
# Install aflak
cargo install --git https://github.com/aflak-vis/aflak aflak

# Done!

# See CLI help
aflak --help

# Open a FITS file with aflak
aflak -f <FITS_FILE>
\end{lstlisting}
