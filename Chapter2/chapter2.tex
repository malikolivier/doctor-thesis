%*******************************************************************************
%****************************** Second Chapter *********************************
%*******************************************************************************

\chapter{Related Works}

\epigraph{
``[A] handful of astronomers who no longer had patience with how outdated
existing tools were and wanted to move to a modern environment began to do
so. [...] The reality is that no current astronomy institution could have
created the equivalent to Astropy, even if funding were available [...]. This
is partly due to institutional culture, partly due to management, and partly
due to the extreme competition for funds. [...] The current code
infrastructure (e.g. IRAF, DS9) is rapidly crumbling, was never designed or
intended to have the longevity being asked of it, and is generations behind
modern design and techniques.''
}{about \emph{Astropy}, by Demitri Muna \emph{et al.}~\citep{muna2016astropy}}

%=== Copy from IEVC2019 submitted pre-print

\section{Viewers and analyzers for astronomical use}
\label{sec:divide}

\subsection{Viewing astronomical datasets}

Astrophysicists use many different kinds of viewers, all of them with their own
idiosyncrasies and specializations within a specific sub-field.
Most of these tools are free and open source software, and we know from
astronomers that the apparently most famous and most used of which is
\mbox{SAOImage}~DS9~\citep{joye2003new}, which can open FITS files and
provide simple analytic tools (a screen capture of DS9 can be found in Figure~\ref{fig:ds9}).
Lately, QFitsView~\cite{ott2012qfitsview} has been gaining traction.
Even some commercial endeavors, such as NightLight, have been
released~\cite{muna2017introducing}.
Kent \emph{et al.}'s undertaking to re-use existing free modeling software such as
Blender to image FITS files deserves notice~\citep{kent2013visualizing}. We
can as well identify new developments to visualize very large datasets that
cannot be loaded into a single modern computer's running memory. As sensor
technology improves, it can only be expected that such tools will become ever
more important in the
future~\citep{perkins2014scalable,hassan2011interactive}.

\begin{figure}
\begin{center}
  \includegraphics[width=0.8\textwidth]{Chapter2/Figs/ds9.png}
  \caption{
      A screen capture of SAOImage DS9. Though DS9 is mainly used as a
      viewer, we can still see some sub-menus related to data analysis.
  }
  \label{fig:ds9}
\end{center}
\end{figure}


Interestingly, the creator of NightLight, Muna, criticizes the current stance
of astrophysicists who only use free software at the expense of productivity.
There are not enough contributors to free software in astronomy, with very few
incentives to become a maintainer.
Indeed, being a maintainer eats an astronomer's precious time, a time that
cannot be used to do research or write papers. Few are willing to incur such
an opportunity cost. This causes software quality to significantly drop,
according to Muna~\citep{muna2016astropy}.
This makes us reflect on our attempt as visualization researcher to build
software for astronomers.

\subsection{Inspiration from non-astronomy viewers}
\label{sec:pyqtgraph}

It is worthy to note that during this research, other data
visualization tools that deal with datacubes were also surveyed. One such tool is
PyQtGraph~\citep{pyqtgraph}, a Python library maintained by Luke Campagnola,
a researcher in neuroscience.
PyQtGraph presents many advantages for exploratory analysis and real-time
visualization compared to \texttt{matplotlib}. It relies on Qt for its
rendering engine and is comparatively faster at redrawing than
\texttt{matplotlib}. Its interface as shown in Figure~\ref{fig:pyqtgraph} is
very intuitive to use.
Unfortunately, it is still limited in speed by the Python language it is
built with. When \aflak{}'s development was first started, it was attempted to extend
PyQtGraph for visualizing astronomical datacubes. However, as soon as the
datasets got more than a few hundreds of megabytes, computing speed started
to linger and did not satisfy \aflak{}'s requirements for real-time analysis,
notwithstanding the attempts to re-write the critical parts of the code in
Cython\footnote{Cython is a compiler for a superset of Python (a sort of
``statically typed'' Python) that allows to write faster code in a Python
environment: \url{https://cython.org/}.}.

\begin{figure}
\begin{center}
  \includegraphics[width=\textwidth]{Chapter2/Figs/pyqtgraph.png}
  \caption{
      A screen capture of PyQtGraph in use. Output windows in \aflak{}'s
      interface to visualize datacubes were inspired from PyQtGraph's
      interface (screen capture from~\citep{pyqtgraph}).
  }
  \label{fig:pyqtgraph}
\end{center}
\end{figure}

\subsection{Tools for data analytics}

Nonetheless, the tools cited in the previous sections are viewers and they do
not provide many data analytics features, if any.
Other tools are in charge of data analytics, the oldest of which being
IRAF~\citep{tody1986iraf}. Then, PyRAF was developed with the successful objective of providing a more user-friendly programming syntax to
the IRAF environment~\citep{de2001pyraf}.
PyRAF, with a Python-based shell syntax,
includes a built-in image algebra and many transformations routinely used in
astrophysics. PyRAF can execute scripts to enable code re-use.
PyRAF, rather than adding feature to IRAF, is more of a new shell around it
that creates a Python-like command-line prompt. We can see an example of IRAF
in use in Listing~\ref{lst:iraf}.
Each IRAF command will take some FITS file(s) as input and output a FITS
file. For example, the command \texttt{imcomb} (for \textbf{im}age
\textbf{comb}ine) in Listing~\ref{lst:iraf}, when the \texttt{combine} flag
is set to \texttt{average} and given a datacube as input, will average
two-dimensional hyperplanes together into a single two-dimensional image. The
other used command, \texttt{imarith} in the example (for \textbf{im}age
\textbf{arith}metic), as its name implies, will do arithmetic operations on FITS
files. In this specific example, it will subtract the values in
\texttt{on-average.fits} from the values in \texttt{off-average.fits}, and
store the result in \texttt{off-on-average.fits}.

\begin{lstlisting}[language=bash, caption={Example of use of IRAF's command-line interface}, label={lst:iraf}]
imcomb @file-off1.list off1-average.fits combine=average
imcomb @file-on.list on-average.fits combine=average
imcomb @file-off2.list off2-average.fits combine=average
imcomb off1-average.fits,off2-average.fits off-average.fits \
  combine=average weight=@scale-off.dat
imarith off-average.fits - on-average.fits off-on-average.fits
\end{lstlisting}


\subsection{Moving to Python}

Then came Astropy, a Python library that solves most of the computing needs
of astrophysicists~\citep{robitaille2013astropy,muna2016astropy}. With the
Python scientific stack (Numpy, SciPy, etc.), it is relatively accessible to
implement some custom analysis in Python.
Astropy is a library that is a actually a collection of smaller astronomical
libraries. For example the PyFITS library~\citep{barrett1999pyfits} was
included in the astropy library as the \texttt{astropy.io.fits} module.
However, this does not solve the problem of the lack of software maintainers
pointed by Muna. Moreover, the Python language is not a general graphical
tool. It requires the learning of a programming language and its ecosystem,
including many external libraries, the adoption of tools such as
\texttt{pip}\footnote{\url{https://pypi.org/project/pip/}} or
\texttt{virtualenv}\footnote{\url{https://pypi.org/project/virtualenv/}} to
download and use such libraries, etc.

Now, we can see the unfortunate divide between visualization and analysis tools
in the astronomy software ecosystem.
An astrophysicist's workflow usually consists in first manually analyzing
datasets by applying and composing transformations on them; only then do they
export the result---usually as a FITS file---to glance at it inside a viewer.
Even for the widely acclaimed Astropy, external libraries (e.g.
\texttt{matplotlib}) are necessary to visualize the results.
\texttt{matplotlib} may be suitable to programmatically generating appealing
figures for publication, but because of the slowness of figure generation
when dealing with datasets more than a few hundreds of megabytes, it is not
suitable for interactive and exploratory analysis.

\section{Visual programming language paradigm}
\label{sec:visual}

Several tools make use of a visual programming approach in order to achieve
better accessibility for domain experts.
AVS/Express or IBM Data Explorer are the pioneers of visual programming
systems for visualization applications, which have been released as
commercial software from the 1990s~\citep{cameron1995special}.
Examples of use of \emph{modular visualization environments} such as AVS/Express
are highly detailed in a paper by Bungartz~\citep{bungartz1998design}, and a
screenshot of the system is shown in Figure~\ref{fig:avs}.
The base of such modular visualization environment systems is that, as
the name indicates, they should be:

\begin{description}
  \item[modular,] in that they include a user-extendable module library;
  \item[visualization,] in that the program output is a visualization;
  \item[environments,] in that they provide a visual programming environment.
\end{description}

Furthermore, Parker explains that SCIRun took this concept further by
extending the dataflow visual paradigm to include numerical
simulations~\citep{parker1995scirun}. And in the same vein, MeVisLab leverages
analogous concepts applied to medical data~\citep{mevislab}.
VisTrails systematizes provenance management with visual graphs
representing the pedigree of the analysis output~\citep{bavoil2005vistrails},
and the same visual approach to workflow management is used by
Kepler~\citep{altintas2006provenance}.
Meyer \emph{et al.} presents Voreen~\citep{meyer2009voreen}, which implements
an extensive graph editor to define volume visualization procedure based on
ray-casting.
OpenAlea is an example of visual programming environment to describe plant
structural models, which allows real time visualization of the \mbox{result~\citep{pradal2008openalea}.}
ViPEr also presents a visual programming system for real-time molecular
visualization~\citep{sanner2002viper}.

\begin{figure}
\begin{center}
  \includegraphics[width=\textwidth]{Chapter2/Figs/avs.png}
  \caption{
      AVS/Express screenshot, from Bungartz \emph{et
      al}~\citep{bungartz1998design}.
  }
  \label{fig:avs}
\end{center}
\end{figure}


KNIME~\citep{berthold2009knime} is another interesting example of analytical
system used for example to create machine learning models in bioscience.
You can see a sample of KNIME's interface in Figure~\ref{fig:knime}.
KNIME is a model in term of easy to read visual programming interface.
It can exports data in several formats, including CSV (Comma-Separated Values)
or JavaScript (using the D3.js\footnote{Data-Driven Documents:
\url{https://d3js.org/}.} visualization library). Nevertheless, KNIME's
transformations have many hidden parameters. Fiddling
with parameters and finding the options the user wants to modify is a
time-consuming process. Besides, rather than recomputing an output in real
time, KNIME's program must be executed using an execution button.

\begin{figure}
\begin{center}
  \includegraphics[width=\textwidth]{Chapter2/Figs/knime.png}
  \caption[KNIME's node editor interface.]{
      KNIME's node editor interface\footnotemark.
  }
  \label{fig:knime}
\end{center}
\end{figure}

\footnotetext{
    Screen capture from
    \url{https://www.knime.com/knime-software/knime-analytics-platform}
    (accessed on August 15\textsuperscript{th} 2019).
}

All these systems, which are designed within the scope of a specific domain
and/or purpose, use the concept of \emph{node} to represent a module through
which the data flows and is transformed.

\section{
    A visual programming approach for viewing and \mbox{analyzing}
    astronomical datasets
}

To get around the shortcomings raised in section~\ref{sec:divide}, \aflak{}'s
objective is to provide a universal collaborative and integrated
environment to analyze and view astronomical data with very fast
iterations~\citep{boussejra2018aflak,boussejra2018aflak2,boussejra2019aflak}.
While \texttt{matplotlib} provides publishing quality graphs, it is far from
suitable for fast iterations on relatively big datasets.  Moreover, there is no
built-in solution for code sharing among researchers.  The best one can do is
to share source files, but then no provenance is supported, reducing
accessibility for convenient reproducible research.

Moreover, \aflak{} follows a visual approach similar to the tools mentioned
in section~\ref{sec:visual}, by combining nodes within a node graph to allow
the user to compose algebraic transforms. Each node has input slots into which
data flows, and a number of output slots from which the transformed data comes
out.  \aflak{} provides an $n$-dimensional image algebra
interface~\citep{ritter1990image}~similar to that of NumPy or IRAF, which can
be used by the user to smoothly visualize the resulting computations.
Other recent frameworks provide analysis tools for visualizing multi-spectral
datasets. This includes BASTet~\cite{rubel2018bastet}, a web-based
visualization environment fine-tuned for mass spectroscopy imaging. However
BASTet does not provide any visual programming feature, contrary to \aflak{}.
The reader may refer to Figure~\ref{fig:equivalent} for a view of \aflak{} in
use. Real-time computing is taken very seriously within \aflak{}: any change
in the node editor must flawlessly trigger a re-computation and a
re-rendering of the visualized output. This sets \aflak{} apart from other
visual programming approaches such as the one laid out with KNIME on the
previous section. What's more, \aflak{} can export the current state of its
node editor and embed it into the end results of the analysis to track
provenance and guarantee reproducibility of the conducted study.

\begin{sidewaysfigure}
\begin{center}
  \makebox[\textwidth][c]{
      \includegraphics[width=\textwidth]{Chapter2/Figs/equivalent_width.eps}
  }
  \caption{
      Example of a semi-complex use of \aflak{} to extract the equivalent width of
      a three-dimensional dataset (refer to Carroll's book for precise
      definition of \emph{equivalent width}~\citep{carroll2007introduction}).
      The \texttt{open\_fits} node opens a FITS file, then several transformations
      are applied to the file to extract the equivalent width in the
      right-most output node (Output~\#3). The result of each output
      node is visualized in a corresponding output window.
      Continuum emission is computed by node~\#4 on the left side, and by node~\#6
      on the right side. The average of the emission line is computed by
      node~\#5. Intermediary results are visualized via output nodes~\#1 and~\#2.
  }
  \label{fig:equivalent}
\end{center}
\end{sidewaysfigure}

\section{Visualization and provenance in forensic science}

In section~\ref{sec:forensics-intro}, we saw the common points between
astronomy and forensic science and an introduction to the development of
the \emph{Legal Medicine Mark-up Language}.

Visualization and computer graphics technologies are being more and more
utilized in many jurisdictions of late. They are especially used to show
injuries or body positions to better understand or demonstrate the succession
of events that led to such traumas~\citep{steele2010beautiful}. Though they
show convincing results, these are \emph{ad~hoc} methods that require someone
competent in computer graphics, because of the heavy manual post-processing
that is needed. Such methods inevitably call for large-scale cooperation
between medical staff and computer
engineers~\citep{urschler2012forensic,benali2013postmortem}.

Therefore, it is desired for the LMML framework to automatize that
cooperation by providing the missing link: a piece of software translating
raw autopsy data as input by the medical examiner to computer graphics, which
can then be appreciated by untrained personnel, including investigators,
prosecutors, lawyers and members of the jury. To that end, these premises require to create
a data model to store, describe and arrange forensic data, thus creating an
\emph{ontology} for computational forensics. Ontology is what ``defines a set
of representational primitives with which to model a domain of knowledge or
discourse''~\citep{gruber2009ontology}.

Hitherto, visualization technologies have mainly been used to help for the
investigation. However, their potential to help in bringing about a judgment
during trial has been neglected until now. Thanks to visualization
technologies, a trial's stakeholders can more easily understand the case at
hand. We believe that such mean would allow to hold members of the jury to be
fairer in their judgment---through a well-informed choice---and their
choosing of the proportioned sentence.

Moreover, the LMML system includes an interface to input forensic data as the
autopsy is being conducted. This is crucial in that, some areas, e.g. Japan,
are lacking competent medical examiners, leading medical staff to be overly
solicited~\citep{okuda2013background}. Examination must thus be performed
quicker to increase efficiency. In short, the contribution of LMML is
three-fold: the design of a semantic language that describes forensic issues,
the conception of an interface to create, edit, analyze or query files
written in that language and the creation of visualizations usable for all
the investigation's stakeholders. Figure~\ref{fig:lmml-input} shows an
example of the authoring interface to define a forensic case, while
Figure~\ref{fig:lmml-output} shows a resulting output visualization generated
from the input content.

Regarding LMML's stake on provenance, provenance management allows to keep
track of whatever was done to solve a case. All the trial and error of the
investigators that led them to the answer would be recorded as well. Thanks
to this, LMML can make possible the reproducibility of an investigation. If a
case were to be re-opened, it is not infrequent that a new investigation must
be conducted. We could thus check exactly how the past investigation was done
and improve the new inspection. Beyond any doubt, provenance management shows
the same benefits in the context of astronomy.

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{Chapter2/Figs/browser-input}
    \caption{\label{fig:lmml-input}
    LMML Browser: Data input interface (head external examination).
    Top: Fixed forms for data relevant to head examination.
    Bottom: Preview of the paragraph dealing with the head included in the
    final forensic report.}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=.9\linewidth]{Chapter2/Figs/VCWS.png}
    \caption{\label{fig:lmml-output}
    LMML Browser: A visualization to explore the result of the autopsy.}
\end{figure}
