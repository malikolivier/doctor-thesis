%*******************************************************************************
%*********************************** First Chapter *****************************
%*******************************************************************************

\chapter{Introduction: How Do Astronomers Analyze Datasets?}  %Title of the First Chapter

\epigraph{``Astronomy is similar to forensic science, in that it relies
entirely on the detection and analysis of the leftovers of past events---mostly consisting of radiation---to reconstruct a plausible explanation for
what is being observed.''}{Author's re-rendering of a speech by Kirk
Borne~\citep{kirk2018massive}}


%********************************** %First Section  **************************************

\section{A collaborative endeavor}

As all scientific disciplines, astrophysics requires deep collaborative support
among researchers in order to make breakthrough results.  The clich\'e of the
lonely researcher hardly exists anymore in real life.
Astrophysicists are expected to provide to their peers reproducible research
and accurate retelling of the analytic process by which a result was achieved.
This cooperation is clearly shown by the ever increasing average number of
co-authors in papers, including for example the 2019 highly mediatized
technological development that allowed the M87* black hole to be directly
imaged. Imaging such a far away object requires a telescope roughly the size
of the Earth in order to mitigate light diffraction on collecting the photons
from an object with an angular size as small as M87*.
Such feat can only be done by creating a virtual telescope through the
cooperation of many telescopes scattered over the whole Earth---the paper
by Akiyama \emph{et al.} that revealed the black hole image boasts 143
different affiliations~\citep{akiyama2019first}.

Interestingly, astronomy is sometimes compared to forensic science in that
astronomers are only able to gather the traces of what is left of far away
physical phenomena, and from there draw conclusions~\citep{kirk2018massive}.
Indeed, all that is left to analyze and understand these phenomena is mostly
the radiation that was emitted from them eons ago\footnote{As a pedantic
addition, apart from electromagnetic radiation, neutrinos or gravitational
waves may as well be detected.}.

In astrophysics, datasets are already publicly shared via open repositories,
with each data sample being assigned a unique identifier, but not the same
can be said about the complete raw analysis process, which involves
everything from the original data to the output that allowed an astronomer to
draw specific conclusions. This of course includes the analytical program
devised by the astronomer, but as well all the steps by which this program
was refined. Hence, we can foresee the need for better sharing practices of
analytical processes and provenance data to improve reproducibility and
potential for collaboration.

\section{Data used by astronomers}

Astrophysical data typically consists of multi-spectral images. Depending on
the specific field of study an astrophysicist may dive in, one may encounter
datasets with three to five dimensions. In addition to the two common spatial
axes (usually right ascension and declination, but other kinds of
domain-specific spacial coordinates, such as galactic coordinates may as well
be used), one may found:

\begin{description}

  \item[Wavelength:] The spacial period of a given ray received by a pixel's
sensor. By taking into account the Doppler effect, radial speed towards the Earth may
be extracted from this value. Thus datasets only containing the resulting
speed may be found (e.g. datasets used by Oka \emph{et
al.}~\citep{oka2017millimetre}).

  \item[Polarization:] A value characteristic of specific milieu or objects
(e.g. \emph{blazars} emit a polarized light~\citep{fujishiro2018timetubes}).

  \item[Time:] Dynamic phenomenon may include time-dependent data (e.g. solar
flare).

\end{description}

Those kinds of multi-dimensional data are represented as datacubes. The most
common format used by astronomers to store and exchange such data is
\emph{Flexible Image Transport System} (FITS)~\citep{wells1979fits}. FITS is
a data format developed in the 70s and 80s for university mainframes, already
from a need to facilitate data exchange between research centers, and has
evolved along the decades it has been in use.
FITS is a very interesting format to investigate for the interested.
Through its evolution, it has remained backward-compatible with its newer extended
features, but still displays peculiar characteristics for the modern observer.
Indeed, FITS was developed to be conveniently recorded on IBM punch-cards,
which were 80 columns long. This is why if one takes a look at the modern
FITS specification, one will see many instances of paddings to fill bits by
multiples of 80 characters. All those rules may seem strange to the
non-initiated, however they make sense when looked through the lens of their
historical backgrounds. This fact alone is of deep intellectual interest for
anyone dealing with a parser or a writer that must support this format.
The author had the privilege to meet with Jessica Mink, the creator of the FITS
format, who also happens to be the discoverer of the rings of Uranus.

A simple FITS file will be separated into two parts: header and data. The
header will contain, as one may expect, metadata regarding the content of the
file\footnote{This is not without reminiscing of DICOM (Digital Imaging and
COmmunication in Medicine) as used in medicine.}.
The data will contain an array of numbers, represented using a little-endian
representation, which is the contrary to the big-endian representation of
numbers now commonly used by our processors. A complying modern FITS parser
will thus require to do bit-wise conversions on floating point representations
of real numbers.

Apart from number crunching, astronomers also deal with data tables. An
extension of FITS allows to encode data tables within the data part of a FITS
file. A standard derived from the representation was codified and is called
ObsCore (Observation Data Model Core Components and its Implementation in the
Table Access Protocol)~\citep{louys2017observation}.

\section{Data retrieval and analysis for the astronomer}

An astronomer's workflow can be broken down into the following four steps:

\begin{enumerate}
    \item Find or query a dataset of interest.
    \item Run some analytical task on the object.
    \item Observe the result of the analytical tasks.
    \item Draw conclusions or go back to any of the previous tasks.
\end{enumerate}

\noindent
Finding and querying datasets can be done using data repositories.
Astronomical data repositories usually expose APIs following certain
standards to allow the user to query datasets according to their properties.
Among such standards, the \emph{Simple Image Access} (SIA) protocol is widely
used~\citep{siap}. Objects for example can be queried depending on their sky
coordinates or luminosity. Query along sky coordinates is usually dubbed
``cone search,'' as even if two objects appear in the same area of the sky,
they can be extremely far from each other, both lying within an extremely
thin cone (with a dimension ranging around the order of a few milliseconds of
arc) stretching from the Earth and passing through the objects.

The astronomer can then use analytical tools to analyze the queried dataset.
Such tool will compute common image algebra on the object (Gaussian fit,
summation, etc.). The array of analytics is very broad and differs widely
between sub-fields of astronomy: a radio-astronomer will not work using the same
analytical primitives as an astronomer specializing in the visible light.
During our research, we attempted to get feedback from as many astronomers as
possible, to avoid alienating any of the tools we develop to a specific
subfield. Indeed, the purpose of our research is to develop ubiquitous and
general tools.

Finally, after analytical tasks are done, the astronomer must visualize the
result of the analysis.
Many different FITS viewers exist for example.
During that phase, there are several types of conclusions to be drawn.
Hopefully, the researcher would be able to find something worthy enough to
become an astronomical paper. However, most of the time this will not be the
case and the result will not be fruitful. The astronomer may instead conclude
that the data they queried does not suit their need, and they will try with
another dataset. Or they may conclude that one of the parameters they used
during analysis was not exactly appropriate, and they may review the
analytical pipeline to start another observation. The point is, whether they
can draw appropriate conclusion or not is dependent on the quality of the
visualization tools they use.

\section{An aside about forensics}
\label{sec:forensics-intro}

Kirk Borne said that ``astronomy is similar to forensic science, in that it
relies entirely on the detection and analysis of the leftovers of past
events---mostly consisting of radiation---to reconstruct a plausible
explanation for what is being observed'' (rephrased by the author).

Forensics is the systematical, scientific method of compiling, analyzing and
profiling data or evidence relevant to the unearthing of past events, in order
to understand what succession of happenstances led to the given consequences.
Let us focus on the \emph{computational} branch of forensic science. The word
``computational'' can actually be appended to a lot of disciplines, e.g.
computational linguistics or computational biology. In the same vein, a body
of knowledge called \emph{computational forensics} can be defined.
Computational methods provide tools that enable a medical examiner or
investigator to better analyze pieces of evidence, otherwise even these
experts would be overwhelmed by the sheer amount of information modern data
gathering schemes provide~\citep{franke2008computational}. The reader is most
certainly already familiar with fingerprint or DNA analysis, which are
examples of practical use of computational forensics methods.

We can see that forensic science and astronomy are not devoid of common
points. The author of this thesis worked on a visualization environment for
forensic medicine, called \emph{Legal Medicine Mark-up Language}
(LMML)~\citep{boussejra2016eurovis,boussejra2016nicograph}. He proposed a
framework for computational forensics that allows quicker and smoother input
of forensic data collected during autopsy (i.e. the medical examination of a
body to uncover the reasons that led to death) through a graphical user
interface specially designed to be used while autopsying. From there, the
framework can output a graphical visualization of the input data, so that the
application be usable and understandable by medical examiners, investigators
and judicial courts alike, each with their own respective point of view.
Hence, the proposed integrated environment serves the specific needs of all
the stakeholders involved in the handling of a forensic case, as shown in
Figure~\ref{fig:lmml-teaser}.

Such a system would allow anyone, from the layman to the hardened
investigator, to have a better understanding of a case. We see that the
challenges that forensic investigators and astronomers face are not without
common points, in that the need of tracking the provenance of the
measurements and pieces of information from which conclusion are drawn is
paramount to explaining the phenomena by which that information was created.

\section{\aflak{}'s introduction}

This thesis presents a free and open source software framework, \aflak{}
(Advanced Framework for Learning Astrophysical Knowledge)\footnote{Interestingly,
the name \aflak{} is a backronym: The signification of the initials was
chosen by Professor Issei Fujishiro after the author came up with the name
\aflak{}. \aflak{} means ``stars'' in Arabic (\aflakar{} \emph{aflāk}). This is a tribute
to the contribution of traditional Arabic astronomers. Even now, many English
names for stars come from Arabic, such as Betelgeuse (from
{\arabicfont الجوزاء}\,{\arabicfont إبط} \emph{Ibṭ al-Jauzā'}) or Aldebaran
(from {\arabicfont الدبران} \emph{ad-Dabarān}).}, which allows interactive
analysis and provenance management on multi-dimensional spectral data, from
the origin of astronomical data stored into dedicated and public repositories
to the final output of the analysis.
\aflak{} is developed and hosted on GitHub at
\texttt{\url{https://github.com/aflak-vis/aflak}}. Binaries can be compiled
from source or downloaded from there.

A free software model was chosen for \aflak{} to promote the free exchange of
programs between research. Indeed, most of current tools in astronomy are
developed by astronomers for astronomers. And it is very much natural for them
to share the tools they use to make discoveries. The author does not want to break from
this model---not because he is not greedy, far from it! Hopefully, his
knowledge as a software engineer can be helpful for astronomers to develop
better tools suited for their problems, and astronomers will in turn be able to
contribute to the development of \aflak{}.

\aflak{} provides a visual programming environment that allows to load a dataset,
to apply transformations on it and to visualize the outputs of these transformations
in real time, thus providing a fast and smooth feedback loop to astronomers.
\aflak{} has built-in support for FITS files (Flexible Image Transport System,
which, as we explained a previous paragraph, has been the \emph{de facto}
standard for astrophysical image for a long time, and includes common data
transformations used by
astronomers~\citep{boussejra2018aflak,boussejra2018aflak2,boussejra2019aflak}.
Visually interacting with the data not only assists the astronomer in finding
particular objects, but it also helps in the design of programs by smoothly
and regularly checking the output, making rapid prototyping of an analysis
pipeline possible.

This thesis will first present the state of the art and the
motivations behind the need for a tool such as \aflak{}, before describing
how the proposed framework contributes to data analytics and provenance
management in astrophysics. Then opinions regarding the
potential for astronomical research in continuing developing such tools
will be expressed.

%-------------------------------------------------------------------------


\begin{figure}
  \includegraphics[width=\linewidth]{Chapter1/figures/teaser}
  \centering
  \caption{
      General workflow for a user of the proposed framework, \aflak{}.
      Datasets can be loaded either from the file system or by issuing
      requests to open data repositories. The visual program can be
      interactively developed while the user can see a visualization of the
      program's output in real time.
  } \label{fig:teaser}
\end{figure}

%-------------------------------------------------------------------------

\begin{figure}
  \includegraphics[width=\linewidth]{Chapter1/figures/lmml-teaser}
  \centering
  \caption{
      General workflow of the \emph{Legal Medicine Mark-up Language}
      framework, with all the involved users.
  }
 \label{fig:lmml-teaser}
\end{figure}
