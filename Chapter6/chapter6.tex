\chapter{Future Works}

\epigraph{
    ``The long term vision is not one of a fixed specific software package,
    but rather one of a framework which enables data centers to provide
    competing and co-operating data services, and which enables software
    providers to offer a variety of compatible analysis and visualization
    tools and user interfaces.''
}
{The Virtual Observatory~\citep{whatisvo}}

\section{Stronger interoperability with VO standards}

\aflak{} allows to record the story of the analysis of astronomical datasets,
and provides a fast and responsive environment for defining custom analytical
macros and sharing them with fellow astronomers. While it currently uses its
own \emph{ad hoc} description of provenance using RON-file based
deserialization, cooperation with the Provenance Data Model working group at
IVOA would be a huge advantage to agree on a common format. This would allow
interoperability between different software that manages analysis workflows,
such as software used for cleaning and denoising raw data observed by
telescopes.

Moreover, more comprehensive integration with other VO standards for querying
would be a major advantage, including support for Simple Cone Search and
Astronomical Data Query Language~\citep{adql}.

\section{Application to other astronomical problems}

\subsection{Arbitrary non-linear slicing}

Some other astronomical problems may be adapted to be solved through
exploratory analysis, as provided by \aflak{}. For the analysis
or discovery of high-velocity clouds such as the ones sighted by Takekawa
\emph{et al.} in our home galaxy~\citep{takekawa2017discovery}, it is very
convenient to be able to make arbitrary, non-linear slicing through
datacubes. Current \aflak{}'s built-in nodes and interface only supports linear
slicing, it would be of great help for researchers to develop such general
tool that allows to define a slicing method while visualizing it. It seems no
such general tool currently exists, if we exclude custom, one-off code. We
think that extending \aflak{} to include this feature is possible, and we
suggest a design to implement such feature in
Figure~\ref{fig:arbitrary-slicing}.
\aflak{}'s development team plans to present this feature at the 2019 Integral Field Spectroscopy Study
Group that will be held in November 2019.

\begin{sidewaysfigure}
    \includegraphics[width=\textwidth]{Chapter6/figures/arbitrary-slicing.eps}
    \centering
    \caption{
      \label{fig:arbitrary-slicing}
      Representation of \aflak{}'s planned extension to support arbitrary
      slicing.
    }
\end{sidewaysfigure}


\subsection{Interferometry}

What's more, interferometry analysis is a typical example of analysis with
many degrees of freedom. We believe that \aflak{} would be adapted to allow
an astronomer to interactively fine-tune the parameters of such analysis.
Images produced by an interferometer are convolved with the Fourier transform
of the data sampling, and are also affected by errors in the gain and phase
calibration, often due to short-term changes in the atmosphere.
AIPS, the \emph{Astronomical Image Processing System}, was designed at about
the same time as FITS was designed.
Its main role has been to put interferometric data calibration, editing,
imaging, and display programs in the hands of
astronomers.
It has provided useful functionality for data from various radio
interferometers and single-dish radio telescopes~\citep{greisen2003aips}.

With the aging of AIPS, which was developed through the 1970s to the 1990s, next
CASA (short for \emph{Common Astronomy Software Applications}) took the role
of main software stack for radio-astronomy analysis~\citep{mcmullin2007casa}.
However, given the size of many datasets obtained with an interferometer, it
may be difficult to maintain fast responsiveness, dear to \aflak{}. On the
other hand, displaying the progress and the intermediary results of the
currently running tasks would then be useful to track advancement and steer
the computation, by offering an implementation of a visualization concept
developed by Johnson \emph{et al.} called ``computational
steering''~\citep{johnson1999interactive}.

\begin{figure}
    \includegraphics[width=\textwidth]{Chapter6/figures/aips.png}
    \centering
    \caption{
      \label{fig:aips}
      Block diagram of AIPS from a user point of view. Various communications
      paths are shown among the main interactive program, AIPS, the batch
      program AIPSB, and the collection of separate tasks (Figure~1
      in~\citep{greisen2003aips}).
    }
\end{figure}

\section{More room for improvement}
\label{sec:more-improvements}

Other limitations include the current feature for selecting region of
interest, which is limited to selecting pixels one at a time. This is
sufficient for many use cases using relatively small datasets, but not in the
general case. \aflak{} does not support very large datasets that do not fit
in the computer's working memory. What's more, \aflak{}'s user interface was
designed with a wide screen in mind---more than 2K~pixels---and, although not
impossible, it is impractical to use \aflak{} on smaller screens.

Furthermore, more memory-efficient use of cache is desired to reduce wasting
of memory and have \aflak{} deal with bigger datasets (more than several
gigabytes). Smarter probabilistic cache that garbage collects values that are
very unlikely to be queried would be a huge improvement.
The speed of the computing could as well be increased by introducing
Just-In-Time (JIT) compilation technologies and/or clever task scheduling,
beseeching us to borrow theoretical concepts from operating systems and compiler
programming.
Besides, on the UI side, being able to group several nodes into a single macro from the node
editor screen would be extremely convenient and is a desirable feature.

We discussed about the issue of sharing the code for custom transformations
made by experienced \aflak{} user in section~\ref{sec:integration}. To
overcome this issue, having a public \aflak{} code repository where anyone
can contribute new nodes would be the solution. When loading a node editor
from an exported FITS (or RON) file, the necessary code would be
automatically downloaded from this public repository so that the analysis can
really be 100\% reproducible on any computer running \aflak{}. Having a
public \aflak{} code repository where anyone can contribute new nodes or
macros is a planned feature. The use of UUIDs to identify macros is a first
step toward building an \aflak{} code repository.
