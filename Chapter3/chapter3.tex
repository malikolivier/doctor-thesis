\chapter{\aflak{}: Advanced Framework for Learning Astronomical Knowledge}
\label{sec:aflak}

\epigraph{``Flow-Based Programming is best understood as a
\emph{coordination} language, rather than as a \emph{programming} language.
Coordination and modularity are two sides of the same coin, and several years
ago Nate Edwards of IBM coined the term ``configurable modularity'' to denote
an ability to re-use independent components just by changing their
interconnections [...]. One of the important characteristics of systems
exhibiting configurable modularity is that you can build them out of ``black
box'' re-usable modules, much like the chips which are used to build logic in
hardware.''}{Paul J. Morrison, IBM~\citep{morrison1994flow}}

\noindent
IBM pioneered visual programming systems for business applications since the
1970s and 1980s, then, visual programming systems for visualization applications have been
implemented from the 1990s, with IBM-developed commercial systems (IBM Data
Explorer), as we saw in section~\ref{sec:visual}.

%=== PacificVAST 2019

\section{Background and motivation}

Data-driven science gathers information from several sources and conducts
non-trivial analysis on it. Keeping track of the provenance of the original
information and of the process behind a successful or failed analysis is
necessary to validate a hypothesis. Astrophysics is such a domain. Though not
domain-specific, general frameworks for provenance management have been
developed for scientific workflow~\cite{altintas2006provenance,
suriarachchi2015komadu}.

While it was not always the case, lately astronomy journals have been
requiring authors to provide the unique identifiers of the original datasets
from which they conducted their analyses. All modern sky surveys and data
publishers categorize and assign unique IDs to astronomical objects. This ID
can for example be cited in academic papers to give credits to the survey
that provided the source. Moreover, most astronomical data publishers
implement methods to issue a query on objects using various standards. In
fact, astronomers maintain a body of standards so that all data publishers
agree on the means of transmission, storage and querying of datasets (and
other resources). This body of standards is known as the \emph{Virtual
Observatory} (VO) and is maintained by the \emph{International Virtual
Observatory Alliance} (IVOA)~\cite{ivoa}. To promote interoperability with
existing software, it is both a necessity and an asset for \aflak{} to be
compliant with the published VO recommendations.

% via various IVOA standards, among them is SIA (Simple Image Access).
Going back to IDs, industrious astronomers will keep track of the identifier
of the objects they study. Where to find such identifier is defined in the
ObsCore protocol in use since~2011~\cite{louys2017observation} (we will come
back to ObsCore later).
 %(\texttt{obs\_publisher\_did}) of since 201X (ObsCore VO standard).
Then they will do some analysis on the object, and publish their results.
During the analysis process, various operations such as transforms or data
extractions are run. From then, a final output data is created. The
astronomer will then publish the final data along with a description of their
analysis. However, how can other scientists be sure that the analysis is
reproducible and that the description is accurate? There are limits to
peer-review, and there is currently no defined standard in astrophysics to
manage provenance. Yet, many astronomers believe defining guidelines for
provenance management is a necessity, and some work has started being
conducted to create standardized methods to describe provenance with a
proposed recommendation for a \emph{Provenance Data
Model}~\cite{servillat2018provenance,galkin2018provenance}). Unfortunately,
there is no common agreement as of today and the project is yet to be
completed.

As a solution to the above problem, the author proposes an \aflak{} extension to the
FITS standard to record provenance within FITS data files, along with an
integration of \aflak{}'s analysis components with astronomical standards for
querying images. Among the various VO standards for querying,
the \emph{Simple Image Access Protocol~2.0} (SIAP)~\cite{siap} was selected to be supported first,
for its relative simplicity and integrability with a node editor interface.

\section{Design goals}

\aflak{} is designed to meet the following requirements.

\subsection{Ease of use and responsiveness}

\aflak{} is designed with ease of use in mind.
Though using \aflak{} requires astronomy-related domain-specific knowledge, it
is designed to be intuitive. Dataflow is clearly indicated by connections
between box-shaped nodes.
The interface is responsive: the output of a visual program is refreshed in
real time as the program is being updated, with very minor delay, so that the
user not be confused about the provenance of the data being shown.
Moreover, errors are shown using a stack trace to show at which exact node
an error occurred.

\subsection{Re-usability and extendability}

\aflak{} can export and import the state of its interface.
Moreover, \aflak{} allows for the import and export of shareable composite
nodes, referred to as ``macro'' hereafter.  Macros can be shared among other
users via a cloud platform or file exchange.
What's more, nodes may be implemented as side-loaded shared libraries, allowing
to create user-defined nodes in a language that supports C/C++ calling
conventions.

\subsection{Collaborative development}

No research is done alone anymore.
\aflak{} aims at making collaborative development and code-sharing as easy as
possible.

\section{System overview}

\subsection{Description}

% DAG (Workflow management)

\aflak{}'s programming interface is composed of a node editor, where nodes can
be freely composed by linking a node's output slot to another node's input slot
with the mouse cursor~\cite{boussejra2018aflak,boussejra2018aflak2}.  The node
editor's left pane contains the node list and the documentation of the
currently selected node. By selecting a node in the node list, the view will
directly jump and center onto that specific node in the node graph.  The node
graph basically reads from left (input) to right (output), in accordance with
the left-to-right convention of most writing systems. The node editor itself
is a representation of dataflow.

There are three different types of nodes:

\begin{description}

  \item[Transformation nodes:] Composable transformation module. Contain a
  specific amount of input slots and output slots, each with a specific
  expected data type. For an example of a transformation node, refer
  to Figure~\ref{fig:aflak-node}.

  \item[Value nodes:] Contain a parameter of a certain data type that can be
  input by the user, or that can be externally set by another tool. Some may
  consider that a value node is a special case of a transformation node
  without any input slot. However, in \aflak{}'s context, they are semantically
  different in that they are not modular ``black boxes,'' as the value in a
  value node can be externally set or edited using the user interface.
  A list of supported types for value nodes can be seen in
  Figure~\ref{fig:aflak-value-nodes}.

  \item[Output nodes:] Final output of the flowing data. Any data type can be
  redirected to an output node. When a new output node is created, a new
  \emph{output window} containing a visualization of the data arriving to
  this node is opened. The output window and the output node share the same
  number. Such output windows can be seen in Figure~\ref{fig:equivalent}.

\end{description}

\begin{figure}
 \makebox[\textwidth][c]{
  \includegraphics[width=\textwidth]{Chapter3/figures/aflak-node.eps}
 }
 \caption{An example of transformation node, which computes the linear composition of
 two images. It has four inputs (on the left): two images $u$, $v$ and two scalar
 parameters $a$ and $b$. The transformation node has a single output slot (on the right)
 from which the image $a u + b v$ comes out. Parameters $a$ and $b$ can either be directly
 input by the user inside the node, or can be taken from the value of an
 output of another node.}
 \label{fig:aflak-node}
\end{figure}

\noindent
A node may as well be the result of the composition of several nodes. This
feature is referred to as \emph{macro}. This will be explained further in
details in section~\ref{sec:macro}.

\subsection{Detailed description of components}


Figure~\ref{fig:aflak-ui} shows \aflak{}'s interface.
The interface has several sub-windows, all of which can be freely moved,
re-sized or minimized (by double-clicking on the window's header).
On the first run, \aflak{} will infer a workable window layout from the
current screen resolution.
On subsequent run, the last used layout---which is user-defined---is re-used.
Indeed, the layout of sub-windows is saved every time \aflak{} is closed.

\subsubsection{Node editor window}

The main sub-window is the node editor, on top of Figure~\ref{fig:aflak-ui}.
The left side of the node editor contains a resizable and minimizable left pane,
which holds two dropdowns: a dropdown containing the list of nodes in the
node editor and a dropdown containing miscellaneous information about the
\emph{active} node. The \emph{active} node is defined as the latest selected
node (several nodes can be selected at the same time by holding the ``Shift''
button~\LKeyShift).
In the node list, each transformation and value node is represented by the
string ``\mbox{\texttt{\#<N> <node name>}},'' where \texttt{N} is the ID the
identifies the node in the node editor. All nodes are assigned a unique ID.
Each output node will be represented as ``\mbox{\texttt{Output \#<N>}}.''
Besides, on clicking on an item in the node list, the node editor will jump
and focus on the clicked node corresponding to the clicked item. This is
convenient to find nodes that are outside of the node editor view.
An enlarged view of the left pane can be seen in Figure~\ref{fig:node-list}.

The ``Active node'' dropdown contains information about the current active
node. First, we can find the node ID, then the node name. Then a short
explanation about what the transformation the node performs. After this
follows a detailed explanation about the expected values as input(s) and
output(s) for the node. Finally, some metadata is displayed, including the
author of the node, the date of creation and its version.


\begin{figure}
 \makebox[\textwidth][c]{
    \includegraphics[width=1.1\textwidth]{Chapter3/figures/aflak-ui.eps}
 }
 \caption{
     General UI of \aflak{}. We can see all of \aflak{}'s components.
     \\ (a) Current node list
     \\ (b) Documentation of last selected node (\emph{active} node)
     \\ (c) Node editor, with output nodes on the left
     \\ (d) Output windows for visualization
 }
 \label{fig:aflak-ui}
\end{figure}

\begin{figure}
\begin{center}
 \includegraphics[width=0.7\textwidth]{Chapter3/figures/node_list.eps}
 \caption{
     Left pane of \aflak{}'s interface, containing the list of nodes in the
     current editor's window.
 }
 \label{fig:node-list}
\end{center}
\end{figure}

The node editor itself consists of a visual directed acyclic node graph. The
user can pan the node editor by holding the left control key~\LKeyCtrl{} while
dragging the mouse on the background of the node editor.
Adding a new node---from value node to output node, including transformation
node---can be done from the ``Add node'' pop-up dialog as shown on
Figure~\ref{fig:aflak-new-node}, which can be opened by right-clicking
anywhere on the background. Nodes can be wired together by dragging the mouse
from an output slot (represented by a circle on the left of a node) to an
input slot (represented by a circle on the right of a node), and vice-versa.
Nodes can be deleted by selecting them with the mouse and pressing
\LKeyDel{}~key (\mbox{\LKeyShift{} + \LKeyBack} on macOS), or by
\LMouseIIR{}~right-clicking and choosing ``Delete'' in the sub-menu.
On addition, a node is added on the nearest available empty space around the
point the cursor of the mouse is located.

Apart from the node graph, the node editor contains both export and import
buttons, which allows to save or load the current start of the editor.
It contains as well a few ergonomic options. For example showing connection
names can disabled to reduce clutter when numerous nodes are displayed at the
same time, allowing the user to only concentrate on visualizing the dataflow.

\begin{sidewaysfigure}
 \makebox[\textwidth][c]{
    \includegraphics[width=\textwidth]{Chapter3/figures/aflak-new-node.png}
 }
 \caption{
    The figure shows the pop-up dialog that allows to select a new node to
    add to the node editor's graph. The pop-up dialog can be opened by
    right-clicking on any empty space in the node editor.
 }
 \label{fig:aflak-new-node}
\end{sidewaysfigure}

\subsubsection{Output windows}

Any type can be redirected to an output node in the node editor.
Each output node is assigned an ID (starting from $1$). For each output node
a corresponding output window is opened to visualize the data flowing into
the output node.
Depending on the data type that goes into the output node, a different
interface will be used for the output window.
Figure~\ref{fig:output-image2d} shows examples of output windows for
visualizing a two-dimensional image.
In any case, each output window has a save feature, inside the menu ``File,''
then ``Save.'' Using the save feature, the content of the output window will
be saved to a FITS or text file. Only multi-dimensional images are saved into
FITS files. Other types will use text files.

\begin{figure}
    \makebox[\textwidth][c]{
        \begin{subfigure}[b]{0.6\textwidth}
            \includegraphics[width=\textwidth]{Chapter3/figures/output-window-image-2d.png}
            \caption{
                A two-dimensional image visualized with sky coordinates used as axis. \\
            }
            \label{fig:output-image2d-skycoords}
        \end{subfigure}
        ~
        \begin{subfigure}[b]{0.6\textwidth}
            \includegraphics[width=\textwidth]{Chapter3/figures/output-window-image-2d-pixels.png}
            \caption{
                A two-dimensional image visualized with pixel coordinates used as axes.
                Pixel coordinates can be selected by toggling the menu ``File,'' then ``Show pixels.''
            }
            \label{fig:output-image2d-pixels}
        \end{subfigure}
    }

    \makebox[\textwidth][c]{
        \begin{subfigure}[b]{0.6\textwidth}
            \includegraphics[width=\textwidth]{Chapter3/figures/output-window-image-2d-swap-lut.png}
            \caption{
                The color Look-Up Table (LUT) can be changed from the ``Swap
                LUT'' menu. \\
            }
            \label{fig:output-image2d-swap-lut}
        \end{subfigure}
        ~
        \begin{subfigure}[b]{0.6\textwidth}
            \includegraphics[width=\textwidth]{Chapter3/figures/output-window-image-2d-change-contrast.png}
            \caption{
                The arrows on the right of the color map can be dragged to
                change contrast. On this image, the background is made darker
                to discern more clearly the galactic core.
            }
            \label{fig:output-image2d-change-contrast}
        \end{subfigure}
    }

    \caption{
        Interfaces of an output window showing a 2D image with several color
        maps and axes.
    }
    \label{fig:output-image2d}
\end{figure}

The design of the output window is partly inspired from PyQtGraph, as was
revealed in section~\ref{sec:pyqtgraph}. On the right, a color bar shows the
mapping from value to colors, along with a histogram showing the distribution
of values. The histogram used logarithmic coordinates, as in astronomy, there
is usually a huge difference (several orders of magnitude) of luminosity
(photon count) between empty areas, a planet or galactic periphery, and a
bright star or galaxy core.
Several color maps are available. They can be selected by right-clicking on
the color bar. The default color map is the ``Flame''
color map shown in Figures~\ref{fig:output-image2d-skycoords}
and~\ref{fig:output-image2d-pixels}.
Figure~\ref{fig:output-image2d-swap-lut} shows an example of the ``GreyClip''
color map, which is very convenient to singularize points above or below a
specific threshold. The user may as well drag the top arrows and bottoms on
the right of the color bar, to change the contrast and selectively decide
what are the more appropriate objects that should be visualized. An example of
change in contrast is shown in Figure~\ref{fig:output-image2d-change-contrast}.


\begin{figure}
    \makebox[\textwidth][c]{
        \begin{subfigure}[b]{0.6\textwidth}
            \includegraphics[width=\textwidth]{Chapter3/figures/output-window-image-2d-axis.png}
            \caption{
                This sub-figure demonstrates the feature to select an axis
                (horizontal or vertical) value of interest on a
                two-dimensional image. \\ \\ \\ \\
            }
            \label{fig:aflak-output-image2d-axis}
        \end{subfigure}
        ~
        \begin{subfigure}[b]{0.6\textwidth}
            \includegraphics[width=\textwidth]{Chapter3/figures/output-window-image-2d-roi.png}
            \caption{
                This sub-figure demonstrates the feature to select region of
                interest on a two-dimensional image. Several regions of
                interest can be drawn on the same image. The ``active''
                region of interest is highlighted on the image and is the one
                which is actually being drawn when the mouse is clicked. The
                user can change the active region of interest at any time via
                the dropdown menu on the bottom.
            }
            \label{fig:aflak-output-image2d-roi}
        \end{subfigure}
    }

    \caption{
        Examples of interaction handles representing a bound value in a
        visualization of a two-dimensional image.
    }
    \label{fig:aflak-output-image2d-handle}
\end{figure}

Besides, the ``two-dimensional image'' view of an output window contains a
feature to select a region of interest, as shown in
Figure~\ref{fig:aflak-output-image2d-roi}, and a feature to select specific
values on the $x$- or $y$-axis (in Figure~\ref{fig:aflak-output-image2d-axis}).


\begin{sidewaysfigure}
 \makebox[\textwidth][c]{
    \includegraphics[width=\textwidth]{Chapter3/figures/output-window-image-1d.png}
 }
 \caption{
    Visualization of the waveform of a datacube. This is the interface of a
    one-dimensional image when displayed in an output window.
    The vertical line is an interactive handle that represents an $x$-axis
    value selected by the user.
 }
 \label{fig:aflak-output-image1d}
\end{sidewaysfigure}

Finally, Figure~\ref{fig:aflak-output-image1d} shows the view of an output
window that renders a one-dimensional image (a waveform) as a curve. The user
can zoom in and out with the wheel of the mouse, and select a value on the
$x$-axis.

It should be as well noted that apart from one- and two-dimensional images,
output windows in current version of \aflak{} can visualize simple types such
as boolean values, integers, floating point numbers, strings of characters,
but also more complex types such as astronomical tables or FITS files (the
content of the FITS header would then be shown).

% Go to new page to improve layout (for now)
\newpage

\subsection{Value nodes, type checking and error handling}

\subsubsection{Value nodes}

A value node could be considered and is in fact the same as a transformation
node without input. However, there is a non-negligible conceptual difference.
Value nodes are not black boxes. Their values can be set from the interface.
Please refer to Figure~\ref{fig:aflak-value-nodes} to see how the value nodes
look like on \aflak{}'s interface. For example, the value node of type
\texttt{Path} contains a local path referring to a file on the user's file
system. It is natural that the value node of a variable of type \texttt{Path}
allows the user to select a file by exposing a file-explorer-like interface.

A value node of type \texttt{Integer} allows the user to input an integer, which
will become the value encased in the node. It is moreover suited for an integer
input interface to provide decrement (\texttt{-}) and increment
(\texttt{+}) buttons.

While a \texttt{Float} value node simply contains a float that can be input
by the user. The same can be said for \texttt{Str} (for ``string'') and
\texttt{Bool} (for ``boolean'').

It should be noted that all values that can be selected and manipulated
inside output windows as shown in Figures~\ref{fig:aflak-output-image1d}
and~\ref{fig:aflak-output-image2d-roi} can be redirected and bound to a value
node and be re-used within the node editor. The binding is \emph{bi-directional}:
editing the value of the node from the node interface will update the
representation of the value in the output window as well.
This allows the user to have fine-grained visual control on several
parameters while designing an analytics pipeline.

\begin{figure}
 \centering
 \includegraphics[width=0.75\textwidth]{Chapter3/figures/value-nodes.png}
 \caption{
     Many types of value nodes as they appear in \aflak{}.
 }
 \label{fig:aflak-value-nodes}
\end{figure}

\subsubsection{Type checking}

\aflak{} is type-checked when the node editor is built. An output slot can
only be attached to an input slot with the same type. If the user wrongfully
attempts to join two incompatible slots together, the action will be aborted
and an explanatory message error similar to the one shown
in Figure~\ref{fig:aflak-error-cyclic} will pop up on the screen.
However, for convenience, some restricted type conversions are also possible.
For example, integer can be converted to float (trivially) and float can be
converted back to integer by rounding. This improves the user experience
as they may wire an output slot of type \texttt{Integer} to an input slot of
type \texttt{Float}.

\subsubsection{Error handling}

There are two types of errors: graph errors and runtime errors.
Graph errors are handled and detected before they actually cause issues.
These may be type errors or doing something that creates an uncomputable
graphs, as shown in Figure~\ref{fig:aflak-error-cyclic}.

\begin{figure}
 \centering
 \includegraphics[width=\textwidth]{Chapter3/figures/aflak-error-cyclic.eps}
 \caption{
    \aflak{} will prevent the user from wasting their CPU resources. No
    cyclic dependencies can be created. The same kind of errors will be
    detected, prevented and a message will be displayed if a user tries to
    nest recursive macros.
 }
 \label{fig:aflak-error-cyclic}
\end{figure}

Runtime errors, on the other hand, are not detected when the graphical
program is created. The output value of a node returning an error will bubble
up, following the dataflow, until an output node. Then the linked output
window will print a stack-trace explaining what kind of error occurred.
Such a runtime error is shown in Figure~\ref{fig:aflak-error-runtime}.

\begin{figure}
 \centering
 \includegraphics[width=\textwidth]{Chapter3/figures/aflak-runtime-error.eps}
 \caption{
     An output window shows a runtime error. The \texttt{fits\_to\_image}
     node cannot find a FITS HDU with index 20. So the error bubbles up to
     Output \#2, and its stack-trace is displayed in the output window for
     easy debugging.
 }
 \label{fig:aflak-error-runtime}
\end{figure}
